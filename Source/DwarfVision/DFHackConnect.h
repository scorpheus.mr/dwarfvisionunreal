// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include <string>
#include "CoreMinimal.h"

class FSocket;

namespace dfproto {
class CoreBindRequest;
class EmptyMessage;
class ListUnitsOut;
class MiniBlock;
} // namespace dfproto

namespace RemoteFortressReader {
class MapInfo;
class UnitList;
class BlockList;
class TiletypeList;
class MaterialList;
class BuildingList;
class PlantRawList;
} // namespace RemoteFortressReader

namespace google {
namespace protobuf {
class MessageLite;
}
} // namespace google

struct RPCMessageHeader {
	static const int MAX_MESSAGE_SIZE = 64 * 1048576;

	int16_t id;
	int32_t size;
};

class UDFHackConnect {
public:
	// Sets default values for this component's properties
	UDFHackConnect();

protected:
	FSocket *socket;
	std::map<std::string, int> cache_id_function;


public:

	void connect();

	void send_message(int id, ::google::protobuf::MessageLite &message_request);
	int32 get_answer_header();
	std::string get_answer();
	int get_id_bind_function(dfproto::CoreBindRequest &message_request);
	std::string get_info_from_dfhack(dfproto::CoreBindRequest &message_request, ::google::protobuf::MessageLite &message_input);
	std::string get_df_version();

	void get_map_info(RemoteFortressReader::MapInfo &out);
	void reset_map_hashes();
	void get_all_unit_list(RemoteFortressReader::UnitList &out);
	void get_list_units(dfproto::ListUnitsOut &out);
	//Not sure it's list in dfhack void get_block(const FVector &pos, dfproto::MiniBlock &out);
	void get_block_complex(const FVector &pos, RemoteFortressReader::BlockList &out);
	void get_block_list(const FVector &p_min, const FVector &p_max, RemoteFortressReader::BlockList &out);
	void get_tiletype_list(RemoteFortressReader::TiletypeList &out);
	void get_material_list(RemoteFortressReader::MaterialList &out);
	void get_building_def_list(RemoteFortressReader::BuildingList &out);
	void get_plant_raw_list(RemoteFortressReader::PlantRawList &out);
};
