// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "DFHackConnect.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "map_actor.generated.h"

class UInstancedStaticMeshComponent;

UCLASS()
class DWARFVISION_API Amap_actor : public AActor {
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	Amap_actor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform &Transform) override;

	UDFHackConnect dfhack_connect;

	std::map<int32, UInstancedStaticMeshComponent *> cache_material_instance;
	std::map<int32, UInstancedStaticMeshComponent *> cache_shape_instance;
};
