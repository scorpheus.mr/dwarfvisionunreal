#include "MyWorldGenerator.h"

//#include "Kismet/GameplayStatics.h"

#include "AllowWindowsPlatformTypes.h"
#include "proto/CoreProtocol.pb.h"
#include "proto/Map.pb.h"
#include "proto/Block.pb.h"
#include "proto/Tile.pb.h"
#include "proto/BasicApi.pb.h"
#include "HideWindowsPlatformTypes.h"

#include "VoxelMaterialBuilder.h"

TVoxelSharedRef<FVoxelWorldGeneratorInstance> UMyWorldGenerator::GetInstance()
{
	return MakeVoxelShared<FVoxelMyWorldGeneratorInstance>(*this);
}

///////////////////////////////////////////////////////////////////////////////

RemoteFortressReader::MapInfo map_info;
RemoteFortressReader::TiletypeList df_tile_type_list;
RemoteFortressReader::MaterialList material_list;
RemoteFortressReader::UnitList unit_list;

RemoteFortressReader::BlockList fresh_blocks_global;

void FVoxelMyWorldGeneratorInstance::Init(const FVoxelWorldGeneratorInit& InitStruct)
{	
	map_info.Clear();
	df_tile_type_list.Clear();
	material_list.Clear();
	unit_list.Clear();

	dfhack_connect.connect();

	auto version = dfhack_connect.get_df_version();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString(version.c_str()));
	dfhack_connect.get_map_info(map_info);
	dfhack_connect.reset_map_hashes();

	// get once to use after(material list is huge)
	dfhack_connect.get_tiletype_list(df_tile_type_list);
	//RemoteFortressReader::PlantRawList plant_list;
	//dfhack_connect.get_plant_raw_list(plant_list);
	dfhack_connect.get_material_list(material_list);

	// get first dwarf position
	dfhack_connect.get_all_unit_list(unit_list);
	// get position on the first dwarf encounter
	FVector dwarf_pos;
	for (auto unit : unit_list.creature_list()) {
		//	if (unit.isvalid()) {
		dwarf_pos = FVector(unit.pos_x(), unit.pos_y(), unit.pos_z());
		break;
		//	}
	}

	// set the camera pos
/*	APlayerController *OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (OurPlayerController)
		OurPlayerController->GetViewTarget()->SetActorLocation(dwarf_pos*101.f);
*/
	FVector min = FVector(0, 0, dwarf_pos.Z - 8);
	FVector max = FVector(map_info.block_size_x(), map_info.block_size_y(), dwarf_pos.Z + 8);
	dfhack_connect.get_block_list(min, max, fresh_blocks_global);

	// create material
	FVoxelMaterialBuilder Builder;
	Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
	Builder.AddMultiIndex(0, 0.5f);
	Builder.AddMultiIndex(1, 0.2f);
	Builder.AddMultiIndex(2, 0.7f);
	auto Material = Builder.Build();

	// load cube instance type
	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(0, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::TREE_MATERIAL] = Material;
		cache_material[RemoteFortressReader::ROOT] = Material;
		cache_material[RemoteFortressReader::MUSHROOM] = Material;
		cache_material[RemoteFortressReader::DRIFTWOOD] = Material;
	}
	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(1, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::GRASS_LIGHT] = Material;
		cache_material[RemoteFortressReader::GRASS_DARK] = Material;
		cache_material[RemoteFortressReader::GRASS_DRY] = Material;
		cache_material[RemoteFortressReader::GRASS_DEAD] = Material;
		cache_material[RemoteFortressReader::PLANT] = Material;
	}

	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(2, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::MINERAL] = Material;
		cache_material[RemoteFortressReader::STONE] = Material;
		cache_material[RemoteFortressReader::SOIL] = Material;
	}

	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(3, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::POOL] = Material;
		cache_material[RemoteFortressReader::BROOK] = Material;
		cache_material[RemoteFortressReader::RIVER] = Material;
		cache_material[RemoteFortressReader::FROZEN_LIQUID] = Material;
	}

	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(4, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::LAVA_STONE] = Material;
		cache_material[RemoteFortressReader::CAMPFIRE] = Material;
		cache_material[RemoteFortressReader::FIRE] = Material;
		cache_material[RemoteFortressReader::ASHES] = Material;
		cache_material[RemoteFortressReader::MAGMA] = Material;
	}

	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(5, 1.f);
		auto Material = Builder.Build();
		cache_material[RemoteFortressReader::CONSTRUCTION] = Material;
	}

	{
		FVoxelMaterialBuilder Builder;
		Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
		Builder.AddMultiIndex(0, 1.f);
		auto Material = Builder.Build();
		cache_shape[RemoteFortressReader::BRANCH] = Material;
		cache_shape[RemoteFortressReader::TRUNK_BRANCH] = Material;
		cache_shape[RemoteFortressReader::TWIG] = Material;
		cache_shape[RemoteFortressReader::SAPLING] = Material;
		cache_shape[RemoteFortressReader::SHRUB] = Material;
	}
}


void FVoxelMyWorldGeneratorInstance::GetValues(TVoxelQueryZone<FVoxelValue>& QueryZone, int32 LOD, const FVoxelItemStack& Items) const
{
	FVector min(QueryZone.Bounds.Min.X/16, QueryZone.Bounds.Min.Y/16, QueryZone.Bounds.Min.Z);
	FVector max(QueryZone.Bounds.Max.X/16, QueryZone.Bounds.Max.Y/16, QueryZone.Bounds.Max.Z);
	dfhack_connect.reset_map_hashes();
	RemoteFortressReader::BlockList fresh_blocks;
	dfhack_connect.get_block_list(min, max, fresh_blocks);
	const auto& blocks = fresh_blocks.map_blocks();

	for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, X))
	{
		for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, Y))
		{
			for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, Z))
			{
				QueryZone.Set(X, Y, Z, FVoxelValue(0.2f));

				if (!blocks.size())
					continue;

				FVector asked_pos(X, Y, Z);
				// find the good block TODO Update this
				for (const auto &fresh_block : blocks) {
					const auto &tiles = fresh_block.tiles();
					if (!tiles.size()) {
						continue;
					}

					FVector world_block_pos(fresh_block.map_x(), fresh_block.map_y(), fresh_block.map_z());

					if (world_block_pos.X <= X && X < world_block_pos.X+16 && 
 						world_block_pos.Y <= Y && Y < world_block_pos.Y+16 && 
						world_block_pos.Z == Z){
						auto tile = tiles[(X - world_block_pos.X) + (Y - world_block_pos.Y)*16];

						if (tile != 0) {
							auto type = df_tile_type_list.tiletype_list(tile);
							const auto& shape = type.shape();
							/*if (shape == RemoteFortressReader::FLOOR ||
								shape == RemoteFortressReader::BOULDER ||
								shape == RemoteFortressReader::PEBBLES ||
								shape == RemoteFortressReader::WALL ||
								shape == RemoteFortressReader::FORTIFICATION ||
								shape == RemoteFortressReader::BROOK_BED ||
								shape == RemoteFortressReader::BROOK_TOP ||
								shape == RemoteFortressReader::TREE_SHAPE ||
								shape == RemoteFortressReader::SAPLING ||
								shape == RemoteFortressReader::SHRUB ||
								shape == RemoteFortressReader::BRANCH ||
								shape == RemoteFortressReader::TRUNK_BRANCH ||
								shape == RemoteFortressReader::TWIG)*/
							if(shape > RemoteFortressReader::EMPTY)
								QueryZone.Set(X, Y, Z, FVoxelValue(-0.2f));
						}
						break;
					}
				}
			}
		}
	}
}

void FVoxelMyWorldGeneratorInstance::GetMaterials(TVoxelQueryZone<FVoxelMaterial>& QueryZone, int32 LOD, const FVoxelItemStack& Items) const
{
	FVector min(QueryZone.Bounds.Min.X/16, QueryZone.Bounds.Min.Y/16, QueryZone.Bounds.Min.Z);
	FVector max(QueryZone.Bounds.Max.X/16, QueryZone.Bounds.Max.Y/16, QueryZone.Bounds.Max.Z);
	dfhack_connect.reset_map_hashes();
	RemoteFortressReader::BlockList fresh_blocks;
	dfhack_connect.get_block_list(min, max, fresh_blocks);
	const auto& blocks = fresh_blocks.map_blocks();


	for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, X))
	{
		for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, Y))
		{
			for (VOXEL_QUERY_ZONE_ITERATE(QueryZone, Z))
			{
				if (!blocks.size())
					continue;

				FVector asked_pos(X, Y, Z);
				// find the good block TODO Update this
				for (const auto &fresh_block : blocks) {
					const auto &tiles = fresh_block.tiles();
					if (!tiles.size()) {
						continue;
					}

					FVector world_block_pos(fresh_block.map_x(), fresh_block.map_y(), fresh_block.map_z());

					if (world_block_pos.X <= X && X < world_block_pos.X+16 && 
						world_block_pos.Y <= Y && Y < world_block_pos.Y+16 && 
						world_block_pos.Z == Z){
						auto tile = tiles[(X - world_block_pos.X) + (Y - world_block_pos.Y)*16];

						if (tile != 0) {
							auto type = df_tile_type_list.tiletype_list(tile);

							if (cache_material.find(type.material()) != cache_material.end()) {
								if (cache_shape.find(type.shape()) != cache_shape.end())
									QueryZone.Set(X, Y, Z, cache_shape[type.shape()]);
								else
									QueryZone.Set(X, Y, Z, cache_material[type.material()]);
							}
						}
						break;
					}
				}
			}
		}
	}
}


v_flt FVoxelMyWorldGeneratorInstance::GetValueImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack &Items) const {
	/*dfproto::MiniBlock block;
	dfhack_connect.get_block(FVector(X/16, Y/16, Z), block);

	FVector asked_pos(X, Y, Z);
	const auto &tiles = block.tile();
	if (!tiles.size())
		return 0.2f; 

	auto tile = tiles[(X - FMath::FloorToInt(X/16)) + (Y - FMath::FloorToInt(Y/16))*16];

	if (tile != 0) {
		auto type = df_tile_type_list.tiletype_list(tile);
		const auto& shape = type.shape();
		/*if (shape == RemoteFortressReader::FLOOR ||
		shape == RemoteFortressReader::BOULDER ||
		shape == RemoteFortressReader::PEBBLES ||
		shape == RemoteFortressReader::WALL ||
		shape == RemoteFortressReader::FORTIFICATION ||
		shape == RemoteFortressReader::BROOK_BED ||
		shape == RemoteFortressReader::BROOK_TOP ||
		shape == RemoteFortressReader::TREE_SHAPE ||
		shape == RemoteFortressReader::SAPLING ||
		shape == RemoteFortressReader::SHRUB ||
		shape == RemoteFortressReader::BRANCH ||
		shape == RemoteFortressReader::TRUNK_BRANCH ||
		shape == RemoteFortressReader::TWIG)*/
	/*	if(shape > RemoteFortressReader::EMPTY)
			return -0.2f;
	}*/

	return 0.2f; 
};

FVoxelMaterial FVoxelMyWorldGeneratorInstance::GetMaterialImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack &Items) const {
	
/*	dfhack_connect.reset_map_hashes();
	dfproto::MiniBlock block;
	dfhack_connect.get_block(FVector(X/16, Y/16, Z), block);

	FVector asked_pos(X, Y, Z);
	const auto &tiles = block.tile();
	if (tiles.size()) {
		auto tile = tiles[(X - FMath::FloorToInt(X / 16)) + (Y - FMath::FloorToInt(Y / 16)) * 16];

		if (tile != 0) {
			auto type = df_tile_type_list.tiletype_list(tile);

			if (cache_material.find(type.material()) != cache_material.end()) {
				if (cache_shape.find(type.shape()) != cache_shape.end())
					return cache_shape[type.shape()];
				else
					return cache_material[type.material()];
			}
		}
	}*/

	/*dfhack_connect.reset_map_hashes();
	RemoteFortressReader::BlockList fresh_blocks;
	dfhack_connect.get_block_complex(FVector(FMath::FloorToInt(X / 16), FMath::FloorToInt(Y / 16), Z), fresh_blocks);
	const auto& blocks = fresh_blocks.map_blocks();
	if (blocks.size()) {
		FVector asked_pos(X, Y, Z);
		const auto& tiles = blocks[0].tiles();
		if (tiles.size()) {
			auto tile = tiles[(X - FMath::FloorToInt(X / 16)*16) + (Y - FMath::FloorToInt(Y / 16)*16) * 16];

			if (tile != 0) {
				auto type = df_tile_type_list.tiletype_list(tile);

				if (cache_material.find(type.material()) != cache_material.end()) {
					if (cache_shape.find(type.shape()) != cache_shape.end())
						return cache_shape[type.shape()];
					else
						return cache_material[type.material()];
				}
			}
		}
	}*/


/*	FVector asked_pos(X, Y, Z);
	// find the good block TODO Update this
	const auto& blocks = fresh_blocks_global.map_blocks();
	for (const auto &fresh_block : blocks) {
		const auto &tiles = fresh_block.tiles();
		if (!tiles.size()) {
			continue;
		}

		FVector world_block_pos(fresh_block.map_x(), fresh_block.map_y(), fresh_block.map_z());

		if (world_block_pos.X <= X && X < world_block_pos.X+16 && 
			world_block_pos.Y <= Y && Y < world_block_pos.Y+16 && 
			world_block_pos.Z == Z){
			auto tile = tiles[(X - world_block_pos.X) + (Y - world_block_pos.Y)*16];

			if (tile != 0) {
				auto type = df_tile_type_list.tiletype_list(tile);

				if (cache_material.find(type.material()) != cache_material.end()) {
					if (cache_shape.find(type.shape()) != cache_shape.end())
						return cache_shape[type.shape()];
					else
						return cache_material[type.material()];
				}
			}
			break;
		}
	}*/
	
	/*
	FVoxelMaterialBuilder Builder;
	Builder.SetMaterialConfig(EVoxelMaterialConfig::MultiIndex);
	Builder.AddMultiIndex(0, 0.5f);
	Builder.AddMultiIndex(1, 0.2f);
	Builder.AddMultiIndex(2, 0.7f);
	auto Material = Builder.Build();
	return Material;*/
	return cache_material[RemoteFortressReader::GRASS_LIGHT];
	//return FVoxelMaterial::CreateFromColor(FColor::Red);
};


TVoxelRange<v_flt> FVoxelMyWorldGeneratorInstance::GetValueRangeImpl(const FIntBox& Bounds, int32 LOD, const FVoxelItemStack& Items) const
{
	// Return the values that GetValueImpl can return in Bounds
	// Used to skip chunks where the value does not change
	// Be careful, if wrong your world will have holes!
	// By default return infinite range to be safe
	return TVoxelRange<v_flt>::Infinite();

	// Example for the GetValueImpl above

	// Noise is between -1 and 1
	const TVoxelRange<v_flt> Height = TVoxelRange<v_flt>(-1, 1);// *NoiseHeight;

	// Z can go from min to max
	TVoxelRange<v_flt> Value = TVoxelRange<v_flt>(Bounds.Min.Z, Bounds.Max.Z) - Height;

	Value /= 5;

	return Value;
}

FVector FVoxelMyWorldGeneratorInstance::GetUpVector(v_flt X, v_flt Y, v_flt Z) const
{
	// Used by spawners
	return FVector::UpVector;
}