// Fill out your copyright notice in the Description page of Project Settings.

#include "map_actor.h"
#include <Runtime/Engine/Classes/Components/StaticMeshComponent.h>
#include <Runtime/Engine/Classes/Components/InstancedStaticMeshComponent.h>
#include <Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h>
#include <Runtime/Engine/Classes/Engine/World.h>
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "Kismet/GameplayStatics.h"


#include "AllowWindowsPlatformTypes.h"
#include "proto/CoreProtocol.pb.h"
#include "proto/Map.pb.h"
#include "proto/RemoteFortressReader.pb.h"
#include "proto/Block.pb.h"
#include "proto/Tile.pb.h"
#include "proto/BasicApi.pb.h"
#include "HideWindowsPlatformTypes.h"

// Sets default values
Amap_actor::Amap_actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SceneComp"));
}


void Amap_actor::OnConstruction(const FTransform& Transform)
{

}

UInstancedStaticMeshComponent *AddInstanceStaticMesh(TCHAR *mesh_path, Amap_actor *map_actor) {
	auto ISMComp = NewObject<UInstancedStaticMeshComponent>(map_actor);
	ISMComp->RegisterComponent();
	ISMComp->SetStaticMesh(LoadObject<UStaticMesh>(nullptr, mesh_path));
	ISMComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ISMComp->SetFlags(RF_Transactional);
	map_actor->AddInstanceComponent(ISMComp);
	return ISMComp;
}

// Called when the game starts or when spawned
void Amap_actor::BeginPlay()
{
	Super::BeginPlay();
	return;
	dfhack_connect.connect();

	auto version = dfhack_connect.get_df_version();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString(version.c_str()));
	RemoteFortressReader::MapInfo map_info;
	dfhack_connect.get_map_info(map_info);
	dfhack_connect.reset_map_hashes();

	// get once to use after(material list is huge)
	RemoteFortressReader::TiletypeList df_tile_type_list;
	dfhack_connect.get_tiletype_list(df_tile_type_list);
	//RemoteFortressReader::PlantRawList plant_list;
	//dfhack_connect.get_plant_raw_list(plant_list);
	RemoteFortressReader::MaterialList material_list;
	dfhack_connect.get_material_list(material_list);

	// get first dwarf position
	RemoteFortressReader::UnitList unit_list;
	dfhack_connect.get_all_unit_list(unit_list);
	// get position on the first dwarf encounter
	FVector dwarf_pos;
	for (auto unit : unit_list.creature_list()) {
		//	if (unit.isvalid()) {
		dwarf_pos = FVector(unit.pos_x(), unit.pos_y(), unit.pos_z());
		break;
		//	}
	}

	// set the camera pos
	APlayerController *OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	if (OurPlayerController)
		OurPlayerController->GetViewTarget()->SetActorLocation(dwarf_pos*101.f);
	
	// load cube instance type
	auto ISMComp = AddInstanceStaticMesh(TEXT("/Game/wood.wood"), this);
	cache_material_instance[RemoteFortressReader::TREE_MATERIAL] = ISMComp;
	cache_material_instance[RemoteFortressReader::ROOT] = ISMComp;
	cache_material_instance[RemoteFortressReader::MUSHROOM] = ISMComp;
	cache_material_instance[RemoteFortressReader::DRIFTWOOD] = ISMComp;

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/grass.grass"), this);
	cache_material_instance[RemoteFortressReader::GRASS_LIGHT] = ISMComp;
	cache_material_instance[RemoteFortressReader::GRASS_DARK] = ISMComp;
	cache_material_instance[RemoteFortressReader::GRASS_DRY] = ISMComp;
	cache_material_instance[RemoteFortressReader::GRASS_DEAD] = ISMComp;
	cache_material_instance[RemoteFortressReader::PLANT] = ISMComp;

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/rock.rock"), this);
	cache_material_instance[RemoteFortressReader::MINERAL] = ISMComp;
	cache_material_instance[RemoteFortressReader::STONE] = ISMComp;
	cache_material_instance[RemoteFortressReader::SOIL] = ISMComp;

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/water.water"), this);
	cache_material_instance[RemoteFortressReader::POOL] = ISMComp;
	cache_material_instance[RemoteFortressReader::BROOK] = ISMComp;
	cache_material_instance[RemoteFortressReader::RIVER] = ISMComp;
	cache_material_instance[RemoteFortressReader::FROZEN_LIQUID] = ISMComp;

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/fire.fire"), this);
	cache_material_instance[RemoteFortressReader::LAVA_STONE] = ISMComp;
	cache_material_instance[RemoteFortressReader::CAMPFIRE] = ISMComp;
	cache_material_instance[RemoteFortressReader::FIRE] = ISMComp;
	cache_material_instance[RemoteFortressReader::ASHES] = ISMComp;
	cache_material_instance[RemoteFortressReader::MAGMA] = ISMComp;

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/construction.construction"), this);
	cache_material_instance[RemoteFortressReader::CONSTRUCTION] = ISMComp;
	/*	FEATURE = 3,
		HFS = 13,
		UNDERWORLD_GATE = 25
	*/

	ISMComp = AddInstanceStaticMesh(TEXT("/Game/foliage.foliage"), this);
	cache_shape_instance[RemoteFortressReader::BRANCH] = ISMComp;
	cache_shape_instance[RemoteFortressReader::TRUNK_BRANCH] = ISMComp;
	cache_shape_instance[RemoteFortressReader::TWIG] = ISMComp;
	cache_shape_instance[RemoteFortressReader::SAPLING] = ISMComp;
	cache_shape_instance[RemoteFortressReader::SHRUB] = ISMComp;
	/*	FLOOR = 1,
	BOULDER = 2,
	PEBBLES = 3,
	WALL = 4,
	FORTIFICATION = 5,
	STAIR_UP = 6,
	STAIR_DOWN = 7,
	STAIR_UPDOWN = 8,
	RAMP = 9,
	RAMP_TOP = 10,
	BROOK_BED = 11,
	BROOK_TOP = 12,
	TREE_SHAPE = 13,
	ENDLESS_PIT = 16,
		*/
	
		FVector min = FVector(0, 0, dwarf_pos.Z - 8);
		FVector max = FVector(map_info.block_size_x(), map_info.block_size_y(), dwarf_pos.Z + 8);
		RemoteFortressReader::BlockList fresh_blocks;
		dfhack_connect.get_block_list(min, max, fresh_blocks);
	/*	
	for (int z = 0; z < map_info.block_size_z(); z += 16) {
		FVector min = FVector(0, 0, z);
		FVector max = FVector(map_info.block_size_x(), map_info.block_size_y(), z+16);
		RemoteFortressReader::BlockList fresh_blocks;
		dfhack_connect.get_block_list(min, max, fresh_blocks);
*/
		for (auto fresh_block : fresh_blocks.map_blocks()) {
			auto world_block_pos = FVector(fresh_block.map_x(), fresh_block.map_y(), fresh_block.map_z());

			int x = 0, y = 0;
			for (auto tile : fresh_block.tiles())
			{
				if (tile != 0) {
					auto type = df_tile_type_list.tiletype_list(tile);

					if (cache_material_instance.find(type.material()) != cache_material_instance.end()) {

						/*		if (type.shape() == RemoteFortressReader::FLOOR)
									tile_shape = type.shape
									block_mat = 1
									color = material_list_color[material.mat_type][material.mat_index]

									// for mat in material_list.material_list:
									// 	if mat.mat_pair.mat_type == material.mat_type and mat.mat_pair.mat_index == material.mat_index:
								// 		break
								else if (type.shape() == RemoteFortressReader::BOULDER)
								tile_shape = type.shape
									block_mat = 3
								else if (type.shape() == RemoteFortressReader::PEBBLES)
									tile_shape = type.shape
									block_mat = 3
								else if (type.shape() == RemoteFortressReader::WALL || type.shape() == RemoteFortressReader::FORTIFICATION )
									tile_shape = type.shape
									block_mat = 3
									iso_array[x, z] = 1
									color = material_list_color[material.mat_type][material.mat_index]
								// if type.material == RemoteFortressReader::PLANT:
									// 	block_mat = 2
								else if (type.shape() == RemoteFortressReader::SHRUB || type.shape() == RemoteFortressReader::SAPLING)
									continue
					*/

						FVector tile_pos((world_block_pos.X + x) * 101.f, (world_block_pos.Y + y) * 101.f, world_block_pos.Z * 101.f);
						FRotator Rotation(0.0f, 0.0f, 0.0f);

						if (cache_shape_instance.find(type.shape()) != cache_shape_instance.end())
							cache_shape_instance[type.shape()]->AddInstance(FTransform(Rotation, tile_pos));
						else
							cache_material_instance[type.material()]->AddInstance(FTransform(Rotation, tile_pos));
					}
				}
				x += 1;
				if (x > 15) {
					x = 0;
					y += 1;
				}
			}
		}
	//}
}

// Called every frame
void Amap_actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

