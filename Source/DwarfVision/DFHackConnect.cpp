// Fill out your copyright notice in the Description page of Project Settings.

#include "DFHackConnect.h"
#include <IPAddress.h>
#include <Interfaces/IPv4/IPv4Address.h>
#include <SocketSubsystem.h>
#include <Sockets.h>

#include "AllowWindowsPlatformTypes.h"
#include "proto/CoreProtocol.pb.h"
#include "proto/Map.pb.h"
#include "proto/RemoteFortressReader.pb.h"
#include "proto/Block.pb.h"
#include "proto/Tile.pb.h"
#include "proto/BasicApi.pb.h"
#include "HideWindowsPlatformTypes.h"


UDFHackConnect::UDFHackConnect() {
}

void UDFHackConnect::connect(){
	cache_id_function.clear();

	socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

	FIPv4Address ip(127, 0, 0, 1);

	TSharedRef<FInternetAddr> addr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	addr->SetIp(ip.Value);
	addr->SetPort(5000);
	bool connected = socket->Connect(*addr);

	// handshake
	uint8 *handshake = new uint8[12];
	memcpy(handshake, "DFHack?\n", sizeof(uint8) * 8);
	handshake[8] = 1;
	handshake[9] = 0;
	handshake[10] = 0;
	handshake[11] = 0;
	int32 size = 12;
	int32 sent = 0;
	bool successful = socket->Send(handshake, size, sent);

	uint32 BytesRead2;
	socket->HasPendingData(BytesRead2);

	int32 BytesRead;
	bool successful_receive = socket->Recv(handshake, 256, BytesRead);
	delete[] handshake;
}

void UDFHackConnect::send_message(int id, ::google::protobuf::MessageLite &message_request) {
	int32 size = message_request.ByteSize();
	int fullsz = size + sizeof(RPCMessageHeader);

	uint8_t *data = new uint8_t[fullsz];
	RPCMessageHeader *hdr = (RPCMessageHeader *)data;

	hdr->id = id;
	hdr->size = size;

	uint8_t *pstart = data + sizeof(RPCMessageHeader);
	uint8_t *pend = message_request.SerializeWithCachedSizesToArray(pstart);
	assert((pend - pstart) == size);

	int32 sent = 0;
	int got = socket->Send(data, fullsz, sent);

	delete[] data;
}

int32 UDFHackConnect::get_answer_header() {
	RPCMessageHeader hdr;
	int32 BytesRead;
	bool successful_receive = socket->Recv((uint8 *)(&hdr), 8, BytesRead);
	return hdr.size; //  size of the waiting message
}

std::string UDFHackConnect::get_answer() {
	int32 size = get_answer_header();
	if (size <= 0)
		return std::string();

	uint8 *data = new uint8[size];
	int32 BytesRead;
	bool successful_receive = socket->Recv(data, size, BytesRead, ESocketReceiveFlags::WaitAll);

	return std::string(data, data + BytesRead);
}

int UDFHackConnect::get_id_bind_function(dfproto::CoreBindRequest &message_request) {
	//  send the query with the header,
	//  0 is the id to call the method bind request
	send_message(0, message_request);

	//  Receive data from the server
	std::string received = get_answer();

	//  receive the answer protobuf containing the id
	dfproto::CoreBindReply out_id;
	out_id.ParseFromString(received);

	return out_id.assigned_id();
}

#include <thread>
#include <mutex>
std::mutex get_from_dfhack_mutex;
std::string UDFHackConnect::get_info_from_dfhack(dfproto::CoreBindRequest &message_request, ::google::protobuf::MessageLite &message_input) {
	std::lock_guard<std::mutex> guard(get_from_dfhack_mutex);

	//  id_function = get_id_bind_function(message_request)
	int id_function;
	if (cache_id_function.find(message_request.method()) != cache_id_function.end())
		id_function = cache_id_function[message_request.method()];
	else
		id_function = get_id_bind_function(message_request);
	cache_id_function[message_request.method()] = id_function;

	//  with the id, call the function and get the result
	send_message(id_function, message_input);

	//  Receive data from the server
	return get_answer();
}

std::string UDFHackConnect::get_df_version() { //  example of how it works, just get the df version
	//  create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method(std::string("GetDFVersion"));
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("dfproto.StringMessage");

	//  the function get_df_version just need an empty protobuf in input message, but need a protobuf
	dfproto::EmptyMessage empty_message;
	std::string data_received = get_info_from_dfhack(message_request, empty_message);

	dfproto::StringMessage out;
	out.ParseFromString(data_received);

	return out.value();
}

void UDFHackConnect::get_map_info(RemoteFortressReader::MapInfo &out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetMapInfo");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.MapInfo");
	message_request.set_plugin("RemoteFortressReader");

	// the function get_map_info just need an empty protobuf in input message, but need a protobuf
	dfproto::EmptyMessage empty_message;
	std::string data_received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(data_received);
}

void UDFHackConnect::reset_map_hashes() {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("ResetMapHashes");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("dfproto.EmptyMessage");
	message_request.set_plugin("RemoteFortressReader");

	// the function get_df_version just need an empty protobuf in input message, but need a protobuf
	dfproto::EmptyMessage empty_message;
	get_info_from_dfhack(message_request, empty_message);
}

void UDFHackConnect::get_all_unit_list(RemoteFortressReader::UnitList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetUnitList");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.UnitList");
	message_request.set_plugin("RemoteFortressReader");

	// the function get_map_info just need an empty protobuf in input message, but need a protobuf
	dfproto::EmptyMessage empty_message;
	std::string data_received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(data_received);
}

void UDFHackConnect::get_list_units(dfproto::ListUnitsOut &out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("ListUnits");
	message_request.set_input_msg("dfproto.ListUnitsIn");
	message_request.set_output_msg("dfproto.ListUnitsOut");

	// input message : which part of datablock we want
	dfproto::ListUnitsIn list_unit_in_message;
	list_unit_in_message.set_scan_all(true);
	list_unit_in_message.set_alive(true);
	list_unit_in_message.set_race(465);
	// list_unit_in_message.mask.labors = true;
	// list_unit_in_message.mask.skills = true;
	// list_unit_in_message.mask.profession = true;
	// list_unit_in_message.mask.misc_traits = true;

	auto received = get_info_from_dfhack(message_request, list_unit_in_message);

	out.ParseFromString(received);
}
/*  Not sure it's list in dfhack
void UDFHackConnect::get_block(const FVector &pos, dfproto::MiniBlock& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetBlock");
	message_request.set_input_msg("RemoteFortressReader.BlockRequest");
	message_request.set_output_msg("dfproto.MiniBlock");
	message_request.set_plugin("RemoteFortressReader");

	// input message : which part of datablock we want
	RemoteFortressReader::BlockRequest input_block_message;
	input_block_message.set_min_x(int(pos.X / 16));
	input_block_message.set_min_y(int(pos.Y / 16));
	input_block_message.set_min_z(int(pos.Z));

	auto received = get_info_from_dfhack(message_request, input_block_message);

	out.ParseFromString(received);
}*/

void UDFHackConnect::get_block_complex(const FVector &pos, RemoteFortressReader::BlockList& out) {
	get_block_list(pos, pos + 1, out);
}

void UDFHackConnect::get_block_list(const FVector &p_min, const FVector &p_max, RemoteFortressReader::BlockList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetBlockList");
	message_request.set_input_msg("RemoteFortressReader.BlockRequest");
	message_request.set_output_msg("RemoteFortressReader.BlockList");
	message_request.set_plugin("RemoteFortressReader");

	// input message : which part of datablock we want
	RemoteFortressReader::BlockRequest input_block_message;
	input_block_message.set_min_x(std::max(0, int(p_min.X)));
	input_block_message.set_max_x(std::max(0, int(p_max.X)));
	input_block_message.set_min_y(std::max(0, int(p_min.Y)));
	input_block_message.set_max_y(std::max(0, int(p_max.Y)));
	input_block_message.set_min_z(std::max(0, int(p_min.Z)));
	input_block_message.set_max_z(std::max(0, int(p_max.Z)));
	input_block_message.set_blocks_needed((input_block_message.max_x() - input_block_message.min_x()) * (input_block_message.max_y() - input_block_message.min_y()) * (input_block_message.max_z() - input_block_message.min_z()));

	// for one block in p_min
	// input_block_message.blocks_needed = 1
	// input_block_message.set_min_x(int(p_min.x / 16)
	// input_block_message.set_max_x(input_block_message.min_x + 1
	// input_block_message.set_min_y(int(p_min.y / 16)
	// input_block_message.set_max_y(input_block_message.min_y + 1
	// input_block_message.set_min_z(int(p_min.z)
	// input_block_message.set_max_z(input_block_message.min_z + 1

	// print("x %d, %d" % (input_block_message.min_x, p_min.x))
	// print("y %d, %d" % (input_block_message.min_y, p_min.y))
	// print("z %d, %d" % (input_block_message.min_z, p_min.z))

	auto received = get_info_from_dfhack(message_request, input_block_message);

	out.ParseFromString(received);
}

void UDFHackConnect::get_tiletype_list(RemoteFortressReader::TiletypeList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetTiletypeList");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.TiletypeList");
	message_request.set_plugin("RemoteFortressReader");

	dfproto::EmptyMessage empty_message;
	auto received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(received);
}

void UDFHackConnect::get_material_list(RemoteFortressReader::MaterialList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetMaterialList");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.MaterialList");
	message_request.set_plugin("RemoteFortressReader");

	dfproto::EmptyMessage empty_message;
	auto received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(received);
}

void UDFHackConnect::get_building_def_list(RemoteFortressReader::BuildingList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetBuildingDefList");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.BuildingList");
	message_request.set_plugin("RemoteFortressReader");

	dfproto::EmptyMessage empty_message;
	auto received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(received);
}

void UDFHackConnect::get_plant_raw_list(RemoteFortressReader::PlantRawList& out) {
	// create function request
	dfproto::CoreBindRequest message_request;
	message_request.set_method("GetPlantRaws");
	message_request.set_input_msg("dfproto.EmptyMessage");
	message_request.set_output_msg("RemoteFortressReader.PlantRawList");
	message_request.set_plugin("RemoteFortressReader");

	dfproto::EmptyMessage empty_message;
	auto received = get_info_from_dfhack(message_request, empty_message);

	out.ParseFromString(received);
}
