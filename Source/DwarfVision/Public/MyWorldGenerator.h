#pragma once

#include <map>
#include "DFHackConnect.h"
#include "CoreMinimal.h"
#include "proto/RemoteFortressReader.pb.h"
#include "VoxelWorldGenerators/VoxelWorldGenerator.h" // Not needed, but that way only need to include this file when declaring a world generator
#include "VoxelWorldGenerators/VoxelWorldGeneratorInstance.h"
#include "MyWorldGenerator.generated.h"

class UInstancedStaticMeshComponent;

UCLASS(Blueprintable)
class UMyWorldGenerator : public UVoxelWorldGenerator
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = "Generator")
		float NoiseHeight = 10.f;

	//~ Begin UVoxelWorldGenerator Interface
	virtual TVoxelSharedRef<FVoxelWorldGeneratorInstance> GetInstance() override;

	//~ End UVoxelWorldGenerator Interface
};

class FVoxelMyWorldGeneratorInstance : public FVoxelWorldGeneratorInstance
{
public:
	GENERATE_MEMBER_FUNCTION_CHECK(GetValueImpl, v_flt, const, v_flt, v_flt, v_flt, int32, const FVoxelItemStack&);
	GENERATE_MEMBER_FUNCTION_CHECK(GetMaterialImpl, FVoxelMaterial, const, v_flt, v_flt, v_flt, int32, const FVoxelItemStack&);
	GENERATE_MEMBER_FUNCTION_CHECK(GetValueRangeImpl, TVoxelRange<v_flt>, const, const FVoxelIntBox&, int32, const FVoxelItemStack&);

	using FVoxelWorldGeneratorInstance::TOutputFunctionPtr;
	using FVoxelWorldGeneratorInstance::TRangeOutputFunctionPtr;

	using FVoxelWorldGeneratorInstance::FBaseFunctionPtrs;
	using FVoxelWorldGeneratorInstance::FCustomFunctionPtrs;

	using UStaticClass = UMyWorldGenerator; 

	explicit FVoxelMyWorldGeneratorInstance(const UMyWorldGenerator& MyGenerator)
		: FVoxelWorldGeneratorInstance(
			UMyWorldGenerator::StaticClass(),
			FBaseFunctionPtrs
			{
				static_cast<TOutputFunctionPtr<v_flt>>(&FVoxelMyWorldGeneratorInstance::GetValueImpl),
				static_cast<TOutputFunctionPtr<FVoxelMaterial>>(&FVoxelMyWorldGeneratorInstance::GetMaterialImpl),
				static_cast<TRangeOutputFunctionPtr<v_flt>>(&FVoxelMyWorldGeneratorInstance::GetValueRangeImpl),
			},
			{})
	{
		// doesn't work with fwd decl static_assert(TIsDerivedFrom<UWorldObject, UVoxelWorldGenerator>::IsDerived, "UWorldObject needs to inherit from UVoxelWorldGenerator");
		static_assert(THasMemberFunction_GetValueImpl   <FVoxelMyWorldGeneratorInstance>::Value, "Missing 'v_flt GetValueImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack& Items) const'");
		static_assert(THasMemberFunction_GetMaterialImpl<FVoxelMyWorldGeneratorInstance>::Value, "Missing 'FVoxelMaterial GetMaterialImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack& Items) const'");
		static_assert(THasMemberFunction_GetValueRangeImpl<FVoxelMyWorldGeneratorInstance>::Value, "Missing 'TVoxelRange<v_flt> GetValueRangeImpl(const FVoxelIntBox& Bounds, int32 LOD, const FVoxelItemStack& Items) const'");
	};

	//~ Begin FVoxelWorldGeneratorInstance Interface
	virtual void Init(const FVoxelWorldGeneratorInit& InitStruct) override;
	TVoxelRange<v_flt> GetValueRangeImpl(const FIntBox& Bounds, int32 LOD, const FVoxelItemStack& Items) const;

	virtual FVector GetUpVector(v_flt X, v_flt Y, v_flt Z) const override final;
	//~ End FVoxelWorldGeneratorInstance Interface


	virtual void GetValues(TVoxelQueryZone<FVoxelValue>& QueryZone, int32 LOD, const FVoxelItemStack& Items) const override;
	virtual void GetMaterials(TVoxelQueryZone<FVoxelMaterial>& QueryZone, int32 LOD, const FVoxelItemStack& Items) const override;


	v_flt GetValueImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack& Items) const;
	FVoxelMaterial GetMaterialImpl(v_flt X, v_flt Y, v_flt Z, int32 LOD, const FVoxelItemStack& Items) const;

	mutable UDFHackConnect dfhack_connect;

	mutable std::map<RemoteFortressReader::TiletypeMaterial, FVoxelMaterial> cache_material;
	mutable std::map<RemoteFortressReader::TiletypeShape, FVoxelMaterial> cache_shape;


private:
};