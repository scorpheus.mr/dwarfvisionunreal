protoc --cpp_out=build Basic.proto BasicApi.proto Block.proto CoreProtocol.proto Material.proto isoworldremote.proto Map.proto Plant.proto RemoteFortressReader.proto rename.proto stockpiles.proto Tile.proto AdventureControl.proto

python regenerateforue4.py build/Basic.pb.cc
python regenerateforue4.py build/BasicApi.pb.cc
python regenerateforue4.py build/Block.pb.cc
python regenerateforue4.py build/CoreProtocol.pb.cc
python regenerateforue4.py build/Material.pb.cc
python regenerateforue4.py build/isoworldremote.pb.cc
python regenerateforue4.py build/Map.pb.cc
python regenerateforue4.py build/Plant.pb.cc
python regenerateforue4.py build/RemoteFortressReader.pb.cc
python regenerateforue4.py build/rename.pb.cc
python regenerateforue4.py build/stockpiles.pb.cc
python regenerateforue4.py build/Tile.pb.cc
python regenerateforue4.py build/AdventureControl.pb.cc
pause
