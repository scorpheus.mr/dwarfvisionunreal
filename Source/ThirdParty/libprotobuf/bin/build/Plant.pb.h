// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Plant.proto

#ifndef PROTOBUF_INCLUDED_Plant_2eproto
#define PROTOBUF_INCLUDED_Plant_2eproto

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3006001
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3006001 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata_lite.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
// @@protoc_insertion_point(includes)
#define PROTOBUF_INTERNAL_EXPORT_protobuf_Plant_2eproto 

namespace protobuf_Plant_2eproto {
// Internal implementation detail -- do not use these members.
struct TableStruct {
  static const ::google::protobuf::internal::ParseTableField entries[];
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[];
  static const ::google::protobuf::internal::ParseTable schema[1];
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
}  // namespace protobuf_Plant_2eproto
namespace dfproto {
class Plant;
class PlantDefaultTypeInternal;
extern PlantDefaultTypeInternal _Plant_default_instance_;
}  // namespace dfproto
namespace google {
namespace protobuf {
template<> ::dfproto::Plant* Arena::CreateMaybeMessage<::dfproto::Plant>(Arena*);
}  // namespace protobuf
}  // namespace google
namespace dfproto {

// ===================================================================

class Plant : public ::google::protobuf::MessageLite /* @@protoc_insertion_point(class_definition:dfproto.Plant) */ {
 public:
  Plant();
  virtual ~Plant();

  Plant(const Plant& from);

  inline Plant& operator=(const Plant& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  Plant(Plant&& from) noexcept
    : Plant() {
    *this = ::std::move(from);
  }

  inline Plant& operator=(Plant&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  inline const ::std::string& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }
  inline ::std::string* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const Plant& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const Plant* internal_default_instance() {
    return reinterpret_cast<const Plant*>(
               &_Plant_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    0;

  void Swap(Plant* other);
  friend void swap(Plant& a, Plant& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline Plant* New() const final {
    return CreateMaybeMessage<Plant>(NULL);
  }

  Plant* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<Plant>(arena);
  }
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from)
    final;
  void CopyFrom(const Plant& from);
  void MergeFrom(const Plant& from);
  void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  void DiscardUnknownFields();
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Plant* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return NULL;
  }
  inline void* MaybeArenaPtr() const {
    return NULL;
  }
  public:

  ::std::string GetTypeName() const final;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required uint32 x = 1;
  bool has_x() const;
  void clear_x();
  static const int kXFieldNumber = 1;
  ::google::protobuf::uint32 x() const;
  void set_x(::google::protobuf::uint32 value);

  // required uint32 y = 2;
  bool has_y() const;
  void clear_y();
  static const int kYFieldNumber = 2;
  ::google::protobuf::uint32 y() const;
  void set_y(::google::protobuf::uint32 value);

  // required bool is_shrub = 3;
  bool has_is_shrub() const;
  void clear_is_shrub();
  static const int kIsShrubFieldNumber = 3;
  bool is_shrub() const;
  void set_is_shrub(bool value);

  // optional uint32 material = 4;
  bool has_material() const;
  void clear_material();
  static const int kMaterialFieldNumber = 4;
  ::google::protobuf::uint32 material() const;
  void set_material(::google::protobuf::uint32 value);

  // @@protoc_insertion_point(class_scope:dfproto.Plant)
 private:
  void set_has_x();
  void clear_has_x();
  void set_has_y();
  void clear_has_y();
  void set_has_is_shrub();
  void clear_has_is_shrub();
  void set_has_material();
  void clear_has_material();

  // helper for ByteSizeLong()
  size_t RequiredFieldsByteSizeFallback() const;

  ::google::protobuf::internal::InternalMetadataWithArenaLite _internal_metadata_;
  ::google::protobuf::internal::HasBits<1> _has_bits_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  ::google::protobuf::uint32 x_;
  ::google::protobuf::uint32 y_;
  bool is_shrub_;
  ::google::protobuf::uint32 material_;
  friend struct ::protobuf_Plant_2eproto::TableStruct;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// Plant

// required uint32 x = 1;
inline bool Plant::has_x() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Plant::set_has_x() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Plant::clear_has_x() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Plant::clear_x() {
  x_ = 0u;
  clear_has_x();
}
inline ::google::protobuf::uint32 Plant::x() const {
  // @@protoc_insertion_point(field_get:dfproto.Plant.x)
  return x_;
}
inline void Plant::set_x(::google::protobuf::uint32 value) {
  set_has_x();
  x_ = value;
  // @@protoc_insertion_point(field_set:dfproto.Plant.x)
}

// required uint32 y = 2;
inline bool Plant::has_y() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void Plant::set_has_y() {
  _has_bits_[0] |= 0x00000002u;
}
inline void Plant::clear_has_y() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void Plant::clear_y() {
  y_ = 0u;
  clear_has_y();
}
inline ::google::protobuf::uint32 Plant::y() const {
  // @@protoc_insertion_point(field_get:dfproto.Plant.y)
  return y_;
}
inline void Plant::set_y(::google::protobuf::uint32 value) {
  set_has_y();
  y_ = value;
  // @@protoc_insertion_point(field_set:dfproto.Plant.y)
}

// required bool is_shrub = 3;
inline bool Plant::has_is_shrub() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void Plant::set_has_is_shrub() {
  _has_bits_[0] |= 0x00000004u;
}
inline void Plant::clear_has_is_shrub() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void Plant::clear_is_shrub() {
  is_shrub_ = false;
  clear_has_is_shrub();
}
inline bool Plant::is_shrub() const {
  // @@protoc_insertion_point(field_get:dfproto.Plant.is_shrub)
  return is_shrub_;
}
inline void Plant::set_is_shrub(bool value) {
  set_has_is_shrub();
  is_shrub_ = value;
  // @@protoc_insertion_point(field_set:dfproto.Plant.is_shrub)
}

// optional uint32 material = 4;
inline bool Plant::has_material() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void Plant::set_has_material() {
  _has_bits_[0] |= 0x00000008u;
}
inline void Plant::clear_has_material() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void Plant::clear_material() {
  material_ = 0u;
  clear_has_material();
}
inline ::google::protobuf::uint32 Plant::material() const {
  // @@protoc_insertion_point(field_get:dfproto.Plant.material)
  return material_;
}
inline void Plant::set_material(::google::protobuf::uint32 value) {
  set_has_material();
  material_ = value;
  // @@protoc_insertion_point(field_set:dfproto.Plant.material)
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__

// @@protoc_insertion_point(namespace_scope)

}  // namespace dfproto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_INCLUDED_Plant_2eproto
